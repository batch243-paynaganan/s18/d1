// console.log("Good afternoon!");

// functions
	// functions in JS are lines/blocks of coldes that tell our devices/application to perform a certain task when called/invoke.
	// functions are mostly created to create complicated tasks to run several lines of codes the same task or functions in succession.
	// They are also used to prevent repeating lines or blocks of codes that contains the same task or function.
	
	// we also learned in the previous session that we can gather data from user using prompt() window.
	
		/*function printInput(){
			let nickname = prompt("Enter your nickname:");
			console.log("Hi " + nickname);
		}

		printInput();*/

	// however, for some use cases, this may not be ideal.
	// for other cases, function can also process data directly passed into it instead of relying Global Variable and prompt()

// parameters and arguments

	// consider this function
		function printName(name = "noName"){
			console.log("My name is " + name);
		}

		printName("Mark");
		printName();

		// You can directly pass data into the function. The function can then use that data which is referred as "name" within the function.
		// "name" is called parameter.
		// parameter acts as named variable/container that exist only inside of a function.
		// "mark" the information/data provided directly into the function called an argument.
		// values passed when invoking a function are called arguments. these arguments are then stored as the parameters within the function.

	// variables can also be passed as an argument.
		let sampleVariable = "Edward";
			printName(sampleVariable);

	// function arguments cannot be used by a function if there are no parameters provided within the function.
			function noParams(){
				let params = "No parameters";

				console.log(params);
			};

			noParams("with parameter!");


		function checkDivisibilityBy8(num){
			let modulo = num % 8;
			console.log("The remainder of " + num + "divided by 8 is: " + modulo);

			let isDivisibleBy8 = modulo === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(8);
		checkDivisibilityBy8(17);

		// you can also do the same using the prompt(), however, take note that prompt() outputs a string. Strings are not ideal for mathematical computations.

	// functions as arguments
		// function parameters can also accept other functions as arguments.
		// some complex functions use other functions as arguments to perform more complicated results.
		// this will be further seen when we discuss arrays methods.

		function argumentFunction(){
			console.log("This function was passed as an argument before the message was printed.");
		};

		function argumentFunctionTwo(){
			console.log("This function was passed as an argument from the second argument function.");
		};

		function invokeFunction(argFunction){
			argFunction();
		};

		/*function invokeFunction(argFunction){
			console.log(argFunction);
		};*/

		invokeFunction(argumentFunction);
		invokeFunction(argumentFunctionTwo);

		// adding and removing the parentheses "()" impacts the output of the JS heavily.
		// when function is used with parentheses "()", it denotes invoking a functions.
		// a function used without parentheses "()", is normally associated with using the function as an argument to another function.

	// Using multiple parameters
		// multiple "arguments" will correspond to the number of "parameters" declared in a function in "succeeding order."

		function createFullName(firstName = "noFirstName", middleName = "noMiddleName", lastName = "noLastName"){
			console.log("This is first name: " + firstName);
			console.log("This is middle name: " + middleName);
			console.log("This is last name: " + lastName);
		};

		createFullName("Juan", "Dela", "Cruz");
		// "juan" will be stored in the parameter "firstName"
		// "dela" will be stored in the parameter "middleName"
		// "cruz" will be stored in the parameter "lastName"

		// in JS, providing more arguments than the expected parameters will not return an error.
 		createFullName("Juan", "Dela", "Cruz", "Jr.");

 		// providing less arguments than the expected parameters will automatically assign an undefined value to the parameter.
		createFullName("Juan", "Dela Cruz");

		// using variables as arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

	// return statement
		// the "return" statement allows us to output a value from a functionto be passed to the line/block of code that invoke the function.

			function returnFullName(firstName, middleName, lastName){
				console.log(firstName + " " + middleName + " " + lastName);
			};

			returnFullName("Ada", "None", "Lovelace");

			function returnName(firstName, middleName, lastName){
				return firstName + " " + middleName + " " + lastName;

				console.log(firstName);
			};

			console.log(returnName("John", "Doe", "Smith"));

			let fullName = returnName("John", "Doe", "Smith");
				console.log("This is the console.log from full name variable:")
				console.log(fullName);

			function printPlayerInfo(username, level, job){
				console.log("Username: " + username);
				console.log("Level: " + level);
				console.log("Job: " + job);

				return username + " " + level + " " + job;
			};

			printPlayerInfo("knight_white", 95, "Paladin");

			let user1 = printPlayerInfo("knight_white", 95, "Paladin");
			console.log(user1);

			